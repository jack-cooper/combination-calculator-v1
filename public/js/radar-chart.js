Vue.component('radar-chart', {
  template: '#radar-chart-template',
  props: {
    size: {
      type: Number
    },
    vertices: {
      type: Array
    }
  },
  computed: {
    // Calculates the vertex positions
    coords: function() {
      var numVertices = this.vertices.length
      var scaleFactor = this.size
      return this.vertices.map(function (vertex, i) {
        var coord = calcVertexCoords(vertex.value * (scaleFactor / 10) , i, numVertices, scaleFactor)
        return coord.x + ',' + coord.y
      }).join(' ')
    },
  }
})

function calcVertexCoords(val, index, numVertices, scaleFactor) {
  var y = val
  var angle = Math.PI * 2 / numVertices * index
  var cos = Math.cos(angle)
  var sin = Math.sin(angle)
  var tx = Math.round(y * sin + scaleFactor / 2)
  var ty = Math.round(-y * cos + scaleFactor / 2)

  return {
    x: tx,
    y: ty
  }
}
