Vue.component('monster-child', {
  template: '#monster-child-template',
  props: {
    initialMainBreed: {
      type: String,
      default: 'Beaclon'
    },
    initialSubBreed: {
      type: String,
      default: 'Beaclon'
    },
    initialBaseStats: {
      type: Array,
      default: function () {
        return zeroStats
      }
    },
    initalHardMode: {
      type: Boolean,
      default: function() {
        return false
      }
    }
  },
  data: function() {
    return {
      mainBreed: this.initialMainBreed,
      subBreed: this.initialSubBreed,
      hardMode: this.iniitalHardMode
    }
  },
  computed: {
    baseStats: function() {
      if (this.exists)
      {
        if (this.hardMode)
        {
          return baseStatsMapHard.get(this.breeds)
        }
        else
        {
          return baseStatsMap.get(this.breeds)
        }
      }
      else
      {
        return zeroStats
      }
    },
    baseStatsOrdered: function() {
      return orderStatArray(this.baseStats, this.baseStats)
    },
    baseStatsCorrected: function() {
      return correctStatArray(this.baseStats, this.gains)
    },
    baseStatsCorrectedOrdered: function() {
      return orderStatArray(this.baseStatsCorrected, this.baseStats)
    },
    breeds: function () {

      var main
      var sub
      if (this.mainBreed == '')
      {
        main = '-'
      }
      else
      {
        main = this.mainBreed
      }
      if (this.subBreed == '')
      {
        sub = '-'
      }
      else
      {
        sub = this.subBreed
      }
      return main + '/' + sub
    },
    exists: function () {
      switch (this.mainBreed)
      {
        case 'Ape':
          switch(this.subBreed)
          {
            case 'Ape':
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Plant':
              return true
          }
          break
        case 'Arrowhead':
          switch(this.subBreed)
          {
            case 'Arrowhead':
            case 'Durahan':
            case 'Golem':
            case 'Henger':
            case 'Joker':
            case 'Mock':
            case 'Suezo':
              return true
          }
          break
        case 'Bajarl':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Joker':
              return true
          }
          break
        case 'Baku':
          switch(this.subBreed)
          {
            case 'Baku':
            case 'Dragon':
            case 'Durahan':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Joker':
            case 'Kato':
            case 'Tiger':
              return true
          }
          break
        case 'Beaclon':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Beaclon':
            case 'Dragon':
            case 'Ducken':
            case 'Durahan':
            case 'Golem':
            case 'Henger':
            case 'Joker':
            case 'Tiger':
              return true
          }
          break
        case 'Centaur':
          switch(this.subBreed)
          {
            case 'Arrowhead':
            case 'Bajarl':
            case 'Centaur':
            case 'Dragon':
            case 'Durahan':
            case 'Golem':
            case 'Joker':
            case 'Pixie':
            case 'Tiger':
              return true
          }
          break
        case 'ColorPandora':
          switch(this.subBreed)
          {
            case 'ColorPandora':
            case 'Jell':
            case 'Pixie':
              return true
          }
          break
        case 'Dragon':
          switch(this.subBreed)
          {
              case 'Arrowhead':
              case 'Bajarl':
              case 'Beaclon':
              case 'Dragon':
              case 'Durahan':
              case 'Gali':
              case 'Golem':
              case 'Henger':
              case 'Joker':
              case 'Kato':
              case 'Metalner':
              case 'Monol':
              case 'Pixie':
              case 'Tiger':
                return true
          }
          break
        case 'Ducken':
          switch(this.subBreed)
          {
            case 'Ducken':
            case 'Golem':
            case 'Suezo':
              return true
          }
          break
        case 'Durahan':
          switch(this.subBreed)
          {
            case 'Arrowhead':
            case 'Beaclon':
            case 'Dragon':
            case 'Durahan':
            case 'Golem':
            case 'Joker':
            case 'Metalner':
            case 'Mock':
            case 'Phoenix':
            case 'Pixie':
            case 'Tiger':
              return true
          }
          break
        case 'Gaboo':
          switch(this.subBreed)
          {
            case 'Gaboo':
            case 'Jell':
            case 'Joker':
            case 'Tiger':
              return true
          }
          break
        case 'Gali':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Golem':
          switch(this.subBreed)
          {
            case 'Arrowhead':
            case 'Bajarl':
            case 'Baku':
            case 'Beaclon':
            case 'Dragon':
            case 'Durahan':
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Henger':
            case 'Jell':
            case 'Joker':
            case 'Metalner':
            case 'Mock':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Wracky':
            case 'Zilla':
            case 'Zuum':
              return true
          }
          break
        case 'Ghost':
          switch(this.subBreed)
          {
            case 'Ghost':
              return true
          }
          break
        case 'Hare':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Henger':
          switch(this.subBreed)
          {
            case 'Dragon':
            case 'Golem':
            case 'Henger':
            case 'Joker':
            case 'Metalner':
            case 'Mock':
            case 'Monol':
            case 'Zuum':
              return true
          }
          break
        case 'Hopper':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Dragon':
            case 'Hopper':
            case 'Jill':
            case 'Joker':
            case 'Kato':
            case 'Metalner':
            case 'Mocchi':
            case 'Mock':
            case 'Pixie':
            case 'Suezo':
            case 'Tiger':
              return true
          }
          break
        case 'Jell':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Jill':
          switch(this.subBreed)
          {
            case 'Hare':
            case 'Jill':
            case 'Joker':
            case 'Kato':
            case 'Pixie':
            case 'Suezo':
            case 'Tiger':
              return true
          }
          break
        case 'Joker':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Dragon':
            case 'Golem':
            case 'Joker':
            case 'Pixie':
            case 'Tiger':
              return true
          }
          break
        case 'Kato':
          switch(this.subBreed)
          {
            case 'Dragon':
            case 'Gali':
            case 'Joker':
            case 'Kato':
            case 'Mocchi':
            case 'Suezo':
            case 'Tiger':
              return true
          }
          break
        case 'Metalner':
          switch(this.subBreed)
          {
            case 'Metalner':
            case 'Pixie':
            case 'Suezo':
              return true
          }
          break
        case 'Mew':
          switch(this.subBreed)
          {
            case 'Hare':
            case 'Jell':
            case 'Mew':
            case 'Pixie':
            case 'Tiger':
              return true
          }
          break
        case 'Mocchi':
          switch(this.subBreed)
          {
            case 'Dragon':
            case 'Durahan':
            case 'Jell':
            case 'Joker':
            case 'Kato':
            case 'Mocchi':
            case 'Pixie':
            case 'Tiger':
              return true
          }
          break
        case 'Mock':
          switch(this.subBreed)
          {
            case 'Joker':
            case 'Mock':
              return true
          }
          break
        case 'Monol':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Naga':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Niton':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Durahan':
            case 'Golem':
            case 'Jell':
            case 'Kato':
            case 'Metalner':
            case 'Mock':
            case 'Niton':
              return true
          }
          break
        case 'Phoenix':
          switch(this.subBreed)
          {
            case 'Phoenix':
            return true
          }
          break
        case 'Pixie':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Centaur':
            case 'Dragon':
            case 'Durahan':
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Jill':
            case 'Joker':
            case 'Kato':
            case 'Metalner':
            case 'Mock':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Wracky':
            case 'Zuum':
              return true
          }
          break
        case 'Plant':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Suezo':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Tiger':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Undine':
          switch(this.subBreed)
          {
            case 'Joker':
            case 'Undine':
              return true
          }
          break
        case 'Worm':
          switch(this.subBreed)
          {
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
        case 'Wracky':
          switch(this.subBreed)
          {
            case 'Bajarl':
            case 'Dragon':
            case 'Durahan':
            case 'Golem':
            case 'Henger':
            case 'Joker':
            case 'Metalner':
            case 'Mock':
            case 'Pixie':
            case 'Wracky':
              return true
          }
          break
        case 'Zilla':
          switch(this.subBreed)
          {
            case 'Jell':
            case 'Pixie':
            case 'Tiger':
            case 'Zilla':
              return true
          }
          break
        case 'Zuum':
          switch(this.subBreed)
          {
            case 'Arrowhead':
            case 'Bajarl':
            case 'Baku':
            case 'Dragon':
            case 'Gali':
            case 'Golem':
            case 'Hare':
            case 'Jell':
            case 'Joker':
            case 'Kato':
            case 'Mock':
            case 'Monol':
            case 'Naga':
            case 'Pixie':
            case 'Plant':
            case 'Suezo':
            case 'Tiger':
            case 'Worm':
            case 'Zuum':
              return true
          }
          break
      }
      return false
    },
    gains: function () {
      if (!this.exists)
      {
        return zeroStats
      }
      else
      {
        var main = this.mainBreed
        var sub = this.subBreed
        if (this.hardMode)
        {
          return avgGains(main + 'Hard', sub + 'Hard')
        }
        else
        {
          switch (main)
          {
            case 'Gali':
              switch (sub)
              {
                case 'Gali':
                  return [
                    { label: 'LIF', value: 2 },
                    { label: 'POW', value: 4 },
                    { label: 'INT', value: 5 },
                    { label: 'SKL', value: 3 },
                    { label: 'SPD', value: 2 },
                    { label: 'DEF', value: 3 },
                  ]
              }
              break
            case 'Golem':
              switch (sub)
              {
              case 'Gali':
                return [
                  { label: 'LIF', value: 3 },
                  { label: 'POW', value: 5 },
                  { label: 'INT', value: 3 },
                  { label: 'SKL', value: 1 },
                  { label: 'SPD', value: 1 },
                  { label: 'DEF', value: 4 },
                ]
              case 'Zilla':
                return [
                  { label: 'LIF', value: 3 },
                  { label: 'POW', value: 4 },
                  { label: 'INT', value: 4 },
                  { label: 'SKL', value: 2 },
                  { label: 'SPD', value: 1 },
                  { label: 'DEF', value: 4 },
                ]
              }
              break
            case 'Henger':
              switch (sub)
              {
                case 'Golem':
                  return [
                    { label: 'LIF', value: 2 },
                    { label: 'POW', value: 4 },
                    { label: 'INT', value: 3 },
                    { label: 'SKL', value: 3 },
                    { label: 'SPD', value: 3 },
                    { label: 'DEF', value: 2 },
                  ]
              }
              break
            case 'Kato':
              switch (sub)
              {
              case 'Joker':
                return [
                  { label: 'LIF', value: 2 },
                  { label: 'POW', value: 2 },
                  { label: 'INT', value: 4 },
                  { label: 'SKL', value: 3 },
                  { label: 'SPD', value: 5 },
                  { label: 'DEF', value: 3 },
                ]
              case 'Mocchi':
                return [
                  { label: 'LIF', value: 2 },
                  { label: 'POW', value: 2 },
                  { label: 'INT', value: 5 },
                  { label: 'SKL', value: 4 },
                  { label: 'SPD', value: 4 },
                  { label: 'DEF', value: 2 },
                ]
              case 'Suezo':
                return [
                  { label: 'LIF', value: 2 },
                  { label: 'POW', value: 2 },
                  { label: 'INT', value: 4 },
                  { label: 'SKL', value: 3 },
                  { label: 'SPD', value: 5 },
                  { label: 'DEF', value: 3 },
                ]
              }
              break
            case 'Mocchi':
              switch (sub)
              {
                case 'Pixie':
                  return [
                    { label: 'LIF', value: 2 },
                    { label: 'POW', value: 3 },
                    { label: 'INT', value: 3 },
                    { label: 'SKL', value: 4 },
                    { label: 'SPD', value: 4 },
                    { label: 'DEF', value: 3 },
                  ]
              }
              break
            case 'Niton':
              switch (sub)
              {
                case 'Mock':
                  return [
                    { label: 'LIF', value: 3 },
                    { label: 'POW', value: 2 },
                    { label: 'INT', value: 3 },
                    { label: 'SKL', value: 2 },
                    { label: 'SPD', value: 2 },
                    { label: 'DEF', value: 4 },
                  ]
              }
              break
            case 'Pixie':
              switch (sub)
              {
                case 'Gali':
                  return [
                    { label: 'LIF', value: 1 },
                    { label: 'POW', value: 2 },
                    { label: 'INT', value: 5 },
                    { label: 'SKL', value: 4 },
                    { label: 'SPD', value: 3 },
                    { label: 'DEF', value: 1 },
                  ]
              }
              break
            case 'Plant':
              switch (sub)
              {
                case 'Tiger':
                  return [
                    { label: 'LIF', value: 4 },
                    { label: 'POW', value: 2 },
                    { label: 'INT', value: 3 },
                    { label: 'SKL', value: 3 },
                    { label: 'SPD', value: 2 },
                    { label: 'DEF', value: 2 },
                  ]
              }
              break
            case 'Suezo':
              switch (sub)
              {
                case 'Golem':
                  return [
                    { label: 'LIF', value: 2 },
                    { label: 'POW', value: 3 },
                    { label: 'INT', value: 5 },
                    { label: 'SKL', value: 4 },
                    { label: 'SPD', value: 2 },
                    { label: 'DEF', value: 2 },
                  ]
              }
              break
            case 'Tiger':
              switch (sub)
              {
                case 'Worm':
                  return [
                    { label: 'LIF', value: 2 },
                    { label: 'POW', value: 2 },
                    { label: 'INT', value: 4 },
                    { label: 'SKL', value: 4 },
                    { label: 'SPD', value: 3 },
                    { label: 'DEF', value: 1 },
                  ]
              }
              break
            case 'Wracky':
              switch (sub)
              {
                case 'Mock':
                  return [
                    { label: 'LIF', value: 3 },
                    { label: 'POW', value: 1 },
                    { label: 'INT', value: 4 },
                    { label: 'SKL', value: 1 },
                    { label: 'SPD', value: 3 },
                    { label: 'DEF', value: 1 },
                  ]
              }
              break
            case 'Zuum':
              switch (sub)
              {
                case 'Dragon':
                  return [
                    { label: 'LIF', value: 2 },
                    { label: 'POW', value: 3 },
                    { label: 'INT', value: 3 },
                    { label: 'SKL', value: 4 },
                    { label: 'SPD', value: 3 },
                    { label: 'DEF', value: 2 },
                  ]
              }
              break
          }
          return avgGains(main, sub)
        }
      }
    },
  },
  methods: {
    maxValue: function(arr) {

      var max = []

      arr.forEach(function(element) {
        max.push(element.value)
      })

      return max.reduce(function(a, b) {
        return Math.max(a, b)
      })
    },
    zeroPad: function(n) {
      var n = Math.abs(n)
      var zeros = Math.max(0, 3 - Math.floor(n).toString().length )
      var zeroString = Math.pow(10, zeros).toString().substr(1)
      if( n < 0 )
      {
          zeroString = '-' + zeroString
      }
      return zeroString + n
    }
  }
})

function avgGains(mainBreed, subBreed) {

  var mainGains = []
  var subGains = []

  gainsMap.get(mainBreed).forEach(function(element) {
    mainGains.push(element.value)
  })

  gainsMap.get(subBreed).forEach(function(element) {
    subGains.push(element.value)
  })

  var avgGains = mainGains.map(function (num, i) {
    return ( (num + subGains[i]) / 2 );
  })

  for (i = 0; i < avgGains.length; i++)
  {
    if (!avgGains[i].isInteger)
    {
      if (mainGains[i] > avgGains[i])
      {
        avgGains[i] = Math.ceil(avgGains[i])
      }
      else
      {
        avgGains[i] = Math.floor(avgGains[i])
      }
    }
  }
  return [
    { label: 'LIF', value: avgGains[0] },
    { label: 'POW', value: avgGains[1] },
    { label: 'INT', value: avgGains[2] },
    { label: 'SKL', value: avgGains[3] },
    { label: 'SPD', value: avgGains[4] },
    { label: 'DEF', value: avgGains[5] },
  ]
}

function correctStatArray(stats, gains) {

  var corrected = []
  var mult = 0
  var cStat = 0
  for(i = 0; i < 6; i++)
  {
    mult = 0
    cStat = stats[i].value * (mult +  0.5 * (gains[i].value - 1))

    if (cStat > 999)
    {
      cStat = 999
    }
    corrected.push(cStat)
  }
  return [
    { label: 'LIF', value: corrected[0] },
    { label: 'POW', value: corrected[1] },
    { label: 'INT', value: corrected[2] },
    { label: 'SKL', value: corrected[3] },
    { label: 'SPD', value: corrected[4] },
    { label: 'DEF', value: corrected[5] },
  ]
}

function orderStatArray(stats, baseStats) {

  var ordered = []

  stats.forEach(function(element) {
    ordered.push(element)
  })

  ordered.sort(function(a, b) {
    var diff = b.value - a.value
    if (diff == 0)
    {
      var idxA
      var idxB

      switch (a.label) {
        case 'LIF':
          idxA = 0
          break
        case 'POW':
          idxA = 1
          break
        case 'INT':
          idxA = 2
          break
        case 'SKL':
          idxA = 3
          break
        case 'SPD':
          idxA = 4
          break
        case 'DEF':
          idxA = 5
          break
      }
      switch (b.label) {
        case 'LIF':
          idxB = 0
          break
        case 'POW':
          idxB = 1
          break
        case 'INT':
          idxB = 2
          break
        case 'SKL':
          idxB = 3
          break
        case 'SPD':
          idxB = 4
          break
        case 'DEF':
          idxB = 5
          break
      }
      diff = baseStats[idxB].value - baseStats[idxA].value
    }
    return diff
  })
  return ordered
}
