var breedList = [
  "Ape",
  "Arrowhead",
  "Bajarl",
  "Baku",
  "Beaclon",
  "Centaur",
  "ColorPandora",
  "Dragon",
  "Ducken",
  "Durahan",
  "Gaboo",
  "Gali",
  "Ghost",
  "Golem",
  "Hare",
  "Henger",
  "Hopper",
  "Jell",
  "Jill",
  "Joker",
  "Kato",
  "Metalner",
  "Mew",
  "Mocchi",
  "Mock",
  "Monol",
  "Naga",
  "Niton",
  "Phoenix",
  "Pixie",
  "Plant",
  "Suezo",
  "Tiger",
  "Undine",
  "Worm",
  "Wracky",
  "Zilla",
  "Zuum",
];

var zeroStats = [
  { label: "LIF", value: 0 },
  { label: "POW", value: 0 },
  { label: "INT", value: 0 },
  { label: "SKL", value: 0 },
  { label: "SPD", value: 0 },
  { label: "DEF", value: 0 },
];

var apeGains = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 4 },
];

var arrowheadGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 5 },
];

var bajarlGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 2 },
];

var bakuGains = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 3 },
];

var beaclonGains = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 4 },
];

var centaurGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var colorPandoraGains = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 2 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 1 },
];

var dragonGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 5 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var duckenGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 5 },
  { label: "DEF", value: 1 },
];

var durahanGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var gabooGains = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var galiGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var ghostGains = [
  { label: "LIF", value: 1 },
  { label: "POW", value: 1 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var golemGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 5 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var hareGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 5 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 5 },
  { label: "DEF", value: 1 },
];

var hengerGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 4 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 2 },
];

var hopperGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 1 },
];

var jellGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 2 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 4 },
];

var jillGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var jokerGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var katoGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 1 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 5 },
  { label: "DEF", value: 2 },
];

var metalnerGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 2 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var mewGains = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 2 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 2 },
];

var mocchiGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 4 },
];

var mockGains = [
  { label: "LIF", value: 1 },
  { label: "POW", value: 2 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var monolGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var nagaGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var nitonGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 2 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 5 },
];

var phoenixGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 1 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 3 },
];

var pixieGains = [
  { label: "LIF", value: 1 },
  { label: "POW", value: 2 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var plantGains = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 1 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 1 },
];

var suezoGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var tigerGains = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 2 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var undineGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 1 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 2 },
];

var wormGains = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 2 },
];

var wrackyGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 1 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var zillaGains = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 5 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var zuumGains = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 3 },
];

var apeGainsHard = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 4 },
];

var arrowheadGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 5 },
];

var bajarlGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 2 },
];

var bakuGainsHard = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 3 },
];

var beaclonGainsHard = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 4 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 4 },
];

var centaurGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var colorPandoraGainsHard = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 3 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var dragonGainsHard = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 5 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 4 },
];

var duckenGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 5 },
  { label: "DEF", value: 1 },
];

var durahanGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var gabooGainsHard = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 5 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var galiGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 4 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var ghostGainsHard = [
  { label: "LIF", value: 1 },
  { label: "POW", value: 3 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var golemGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 5 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var hareGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 5 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 5 },
  { label: "DEF", value: 1 },
];

var hengerGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 4 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 2 },
];

var hopperGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 1 },
];

var jellGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 2 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 4 },
];

var jillGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 4 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var jokerGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var katoGainsHard = [
  { label: "LIF", value: 1 },
  { label: "POW", value: 2 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 5 },
  { label: "DEF", value: 2 },
];

var metalnerGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 2 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var mewGainsHard = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 2 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 2 },
];

var mocchiGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 5 },
];

var mockGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 2 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var monolGainsHard = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 5 },
];

var nagaGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 5 },
  { label: "INT", value: 1 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var nitonGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 2 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 5 },
];

var phoenixGainsHard = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 1 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var pixieGainsHard = [
  { label: "LIF", value: 1 },
  { label: "POW", value: 2 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var plantGainsHard = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 1 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 1 },
];

var suezoGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 3 },
  { label: "INT", value: 5 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 2 },
];

var tigerGainsHard = [
  { label: "LIF", value: 2 },
  { label: "POW", value: 2 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var undineGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 1 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 5 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var wormGainsHard = [
  { label: "LIF", value: 5 },
  { label: "POW", value: 3 },
  { label: "INT", value: 3 },
  { label: "SKL", value: 3 },
  { label: "SPD", value: 1 },
  { label: "DEF", value: 2 },
];

var wrackyGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 1 },
  { label: "INT", value: 4 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 4 },
  { label: "DEF", value: 1 },
];

var zillaGainsHard = [
  { label: "LIF", value: 4 },
  { label: "POW", value: 5 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 1 },
  { label: "SPD", value: 2 },
  { label: "DEF", value: 3 },
];

var zuumGainsHard = [
  { label: "LIF", value: 3 },
  { label: "POW", value: 3 },
  { label: "INT", value: 2 },
  { label: "SKL", value: 4 },
  { label: "SPD", value: 3 },
  { label: "DEF", value: 3 },
];
var gainsMap = new Map();

gainsMap.set("Ape", apeGains);
gainsMap.set("Arrowhead", arrowheadGains);
gainsMap.set("Bajarl", bajarlGains);
gainsMap.set("Baku", bakuGains);
gainsMap.set("Beaclon", beaclonGains);
gainsMap.set("Centaur", centaurGains);
gainsMap.set("ColorPandora", colorPandoraGains);
gainsMap.set("Dragon", dragonGains);
gainsMap.set("Ducken", duckenGains);
gainsMap.set("Durahan", durahanGains);
gainsMap.set("Gaboo", gabooGains);
gainsMap.set("Gali", galiGains);
gainsMap.set("Ghost", ghostGains);
gainsMap.set("Golem", golemGains);
gainsMap.set("Hare", hareGains);
gainsMap.set("Henger", hengerGains);
gainsMap.set("Hopper", hopperGains);
gainsMap.set("Jell", jellGains);
gainsMap.set("Jill", jillGains);
gainsMap.set("Joker", jokerGains);
gainsMap.set("Kato", katoGains);
gainsMap.set("Metalner", metalnerGains);
gainsMap.set("Mew", mewGains);
gainsMap.set("Mocchi", mocchiGains);
gainsMap.set("Mock", mockGains);
gainsMap.set("Monol", monolGains);
gainsMap.set("Naga", nagaGains);
gainsMap.set("Niton", nitonGains);
gainsMap.set("Phoenix", phoenixGains);
gainsMap.set("Pixie", pixieGains);
gainsMap.set("Plant", plantGains);
gainsMap.set("Suezo", suezoGains);
gainsMap.set("Tiger", tigerGains);
gainsMap.set("Undine", undineGains);
gainsMap.set("Worm", wormGains);
gainsMap.set("Wracky", wrackyGains);
gainsMap.set("Zilla", zillaGains);
gainsMap.set("Zuum", zuumGains);
gainsMap.set("ApeHard", apeGainsHard);
gainsMap.set("ArrowheadHard", arrowheadGainsHard);
gainsMap.set("BajarlHard", bajarlGainsHard);
gainsMap.set("BakuHard", bakuGainsHard);
gainsMap.set("BeaclonHard", beaclonGainsHard);
gainsMap.set("CentaurHard", centaurGainsHard);
gainsMap.set("ColorPandoraHard", colorPandoraGainsHard);
gainsMap.set("DragonHard", dragonGainsHard);
gainsMap.set("DuckenHard", duckenGainsHard);
gainsMap.set("DurahanHard", durahanGainsHard);
gainsMap.set("GabooHard", gabooGainsHard);
gainsMap.set("GaliHard", galiGainsHard);
gainsMap.set("GhostHard", ghostGainsHard);
gainsMap.set("GolemHard", golemGainsHard);
gainsMap.set("HareHard", hareGainsHard);
gainsMap.set("HengerHard", hengerGainsHard);
gainsMap.set("HopperHard", hopperGainsHard);
gainsMap.set("JellHard", jellGainsHard);
gainsMap.set("JillHard", jillGainsHard);
gainsMap.set("JokerHard", jokerGainsHard);
gainsMap.set("KatoHard", katoGainsHard);
gainsMap.set("MetalnerHard", metalnerGainsHard);
gainsMap.set("MewHard", mewGainsHard);
gainsMap.set("MocchiHard", mocchiGainsHard);
gainsMap.set("MockHard", mockGainsHard);
gainsMap.set("MonolHard", monolGainsHard);
gainsMap.set("NagaHard", nagaGainsHard);
gainsMap.set("NitonHard", nitonGainsHard);
gainsMap.set("PhoenixHard", phoenixGainsHard);
gainsMap.set("PixieHard", pixieGainsHard);
gainsMap.set("PlantHard", plantGainsHard);
gainsMap.set("SuezoHard", suezoGainsHard);
gainsMap.set("TigerHard", tigerGainsHard);
gainsMap.set("UndineHard", undineGainsHard);
gainsMap.set("WormHard", wormGainsHard);
gainsMap.set("WrackyHard", wrackyGainsHard);
gainsMap.set("ZillaHard", zillaGainsHard);
gainsMap.set("ZuumHard", zuumGainsHard);

var baseStatsMapPrelim = new Map();

baseStatsMapPrelim.set("Ape/Ape", [150, 160, 20, 120, 100, 140]);
baseStatsMapPrelim.set("Ape/Gali", [120, 140, 110, 100, 90, 130]);
baseStatsMapPrelim.set("Ape/Golem", [150, 160, 70, 100, 90, 140]);
baseStatsMapPrelim.set("Ape/Hare", [160, 140, 60, 110, 150, 120]);
baseStatsMapPrelim.set("Ape/Plant", [140, 120, 90, 110, 100, 130]);
baseStatsMapPrelim.set("Arrowhead/Arrowhead", [120, 80, 70, 30, 40, 170]);
baseStatsMapPrelim.set("Arrowhead/Durahan", [130, 120, 80, 110, 100, 170]);
baseStatsMapPrelim.set("Arrowhead/Golem", [120, 150, 70, 50, 60, 190]);
baseStatsMapPrelim.set("Arrowhead/Henger", [120, 100, 60, 110, 90, 130]);
baseStatsMapPrelim.set("Arrowhead/Joker", [100, 110, 120, 140, 70, 150]);
baseStatsMapPrelim.set("Arrowhead/Mock", [90, 110, 130, 100, 60, 150]);
baseStatsMapPrelim.set("Arrowhead/Suezo", [100, 120, 130, 110, 70, 140]);
baseStatsMapPrelim.set("Bajarl/Bajarl", [100, 130, 90, 120, 110, 80]);
baseStatsMapPrelim.set("Bajarl/Joker", [100, 150, 120, 140, 110, 90]);
baseStatsMapPrelim.set("Baku/Baku", [180, 130, 50, 70, 60, 150]);
baseStatsMapPrelim.set("Baku/Dragon", [150, 130, 100, 80, 60, 120]);
baseStatsMapPrelim.set("Baku/Durahan", [150, 140, 70, 80, 50, 160]);
baseStatsMapPrelim.set("Baku/Golem", [160, 130, 70, 90, 60, 150]);
baseStatsMapPrelim.set("Baku/Hare", [150, 130, 50, 90, 100, 80]);
baseStatsMapPrelim.set("Baku/Jell", [150, 120, 90, 100, 70, 130]);
baseStatsMapPrelim.set("Baku/Joker", [160, 140, 100, 110, 70, 120]);
baseStatsMapPrelim.set("Baku/Kato", [160, 110, 120, 70, 100, 130]);
baseStatsMapPrelim.set("Baku/Tiger", [150, 120, 50, 100, 70, 90]);
baseStatsMapPrelim.set("Beaclon/Bajarl", [120, 150, 50, 100, 90, 110]);
baseStatsMapPrelim.set("Beaclon/Beaclon", [120, 150, 50, 70, 90, 140]);
baseStatsMapPrelim.set("Beaclon/Dragon", [120, 150, 110, 70, 90, 140]);
baseStatsMapPrelim.set("Beaclon/Ducken", [90, 150, 70, 100, 120, 110]);
baseStatsMapPrelim.set("Beaclon/Durahan", [120, 150, 80, 70, 90, 140]);
baseStatsMapPrelim.set("Beaclon/Golem", [120, 150, 80, 70, 90, 140]);
baseStatsMapPrelim.set("Beaclon/Henger", [100, 150, 80, 110, 120, 130]);
baseStatsMapPrelim.set("Beaclon/Joker", [120, 150, 130, 100, 90, 110]);
baseStatsMapPrelim.set("Beaclon/Tiger", [100, 110, 70, 130, 90, 120]);
baseStatsMapPrelim.set("Centaur/Arrowhead", [90, 100, 110, 140, 150, 120]);
baseStatsMapPrelim.set("Centaur/Bajarl", [90, 100, 110, 160, 150, 80]);
baseStatsMapPrelim.set("Centaur/Centaur", [90, 100, 140, 160, 150, 80]);
baseStatsMapPrelim.set("Centaur/Dragon", [90, 130, 140, 120, 150, 70]);
baseStatsMapPrelim.set("Centaur/Durahan", [90, 100, 140, 130, 150, 120]);
baseStatsMapPrelim.set("Centaur/Golem", [90, 130, 140, 100, 150, 110]);
baseStatsMapPrelim.set("Centaur/Joker", [90, 100, 140, 160, 150, 80]);
baseStatsMapPrelim.set("Centaur/Pixie", [70, 100, 140, 160, 110, 80]);
baseStatsMapPrelim.set("Centaur/Tiger", [90, 100, 140, 160, 170, 80]);
baseStatsMapPrelim.set("ColorPandora/ColorPandora", [
  170,
  50,
  30,
  100,
  110,
  60,
]);
baseStatsMapPrelim.set("ColorPandora/Jell", [130, 50, 80, 100, 110, 90]);
baseStatsMapPrelim.set("ColorPandora/Pixie", [90, 40, 120, 100, 110, 30]);
baseStatsMapPrelim.set("Dragon/Arrowhead", [100, 150, 140, 120, 90, 130]);
baseStatsMapPrelim.set("Dragon/Bajarl", [100, 170, 130, 120, 90, 110]);
baseStatsMapPrelim.set("Dragon/Beaclon", [100, 170, 130, 120, 90, 110]);
baseStatsMapPrelim.set("Dragon/Dragon", [100, 170, 160, 120, 90, 110]);
baseStatsMapPrelim.set("Dragon/Durahan", [100, 170, 140, 120, 90, 130]);
baseStatsMapPrelim.set("Dragon/Gali", [100, 130, 160, 120, 90, 110]);
baseStatsMapPrelim.set("Dragon/Golem", [100, 170, 140, 80, 90, 130]);
baseStatsMapPrelim.set("Dragon/Henger", [100, 170, 140, 120, 130, 110]);
baseStatsMapPrelim.set("Dragon/Joker", [100, 130, 160, 140, 90, 110]);
baseStatsMapPrelim.set("Dragon/Kato", [100, 140, 160, 120, 130, 110]);
baseStatsMapPrelim.set("Dragon/Metalner", [70, 100, 110, 140, 90, 150]);
baseStatsMapPrelim.set("Dragon/Monol", [100, 150, 140, 120, 90, 130]);
baseStatsMapPrelim.set("Dragon/Pixie", [80, 130, 170, 120, 100, 110]);
baseStatsMapPrelim.set("Dragon/Tiger", [100, 140, 160, 130, 90, 80]);
baseStatsMapPrelim.set("Ducken/Ducken", [70, 80, 130, 140, 100, 50]);
baseStatsMapPrelim.set("Ducken/Golem", [70, 150, 120, 130, 100, 110]);
baseStatsMapPrelim.set("Ducken/Suezo", [80, 100, 130, 150, 140, 50]);
baseStatsMapPrelim.set("Durahan/Arrowhead", [120, 140, 90, 110, 50, 160]);
baseStatsMapPrelim.set("Durahan/Beaclon", [100, 150, 80, 110, 30, 180]);
baseStatsMapPrelim.set("Durahan/Dragon", [100, 130, 150, 110, 70, 140]);
baseStatsMapPrelim.set("Durahan/Durahan", [100, 150, 80, 110, 70, 180]);
baseStatsMapPrelim.set("Durahan/Golem", [120, 150, 100, 90, 60, 170]);
baseStatsMapPrelim.set("Durahan/Joker", [100, 120, 150, 140, 80, 130]);
baseStatsMapPrelim.set("Durahan/Metalner", [90, 120, 80, 140, 70, 160]);
baseStatsMapPrelim.set("Durahan/Mock", [90, 120, 130, 110, 70, 150]);
baseStatsMapPrelim.set("Durahan/Phoenix", [100, 120, 130, 110, 90, 150]);
baseStatsMapPrelim.set("Durahan/Pixie", [90, 110, 150, 130, 100, 120]);
baseStatsMapPrelim.set("Durahan/Tiger", [130, 110, 120, 140, 90, 100]);
baseStatsMapPrelim.set("Gaboo/Gaboo", [190, 120, 30, 40, 150, 70]);
baseStatsMapPrelim.set("Gaboo/Jell", [140, 130, 80, 70, 120, 90]);
baseStatsMapPrelim.set("Gaboo/Joker", [150, 140, 130, 110, 100, 40]);
baseStatsMapPrelim.set("Gaboo/Tiger", [160, 120, 60, 100, 150, 50]);
baseStatsMapPrelim.set("Gali/Gali", [110, 130, 160, 120, 90, 100]);
baseStatsMapPrelim.set("Gali/Golem", [90, 160, 120, 110, 100, 150]);
baseStatsMapPrelim.set("Gali/Hare", [100, 150, 110, 130, 120, 90]);
baseStatsMapPrelim.set("Gali/Jell", [90, 120, 150, 110, 80, 100]);
baseStatsMapPrelim.set("Gali/Monol", [90, 110, 130, 120, 100, 140]);
baseStatsMapPrelim.set("Gali/Naga", [90, 130, 140, 120, 100, 110]);
baseStatsMapPrelim.set("Gali/Pixie", [100, 130, 170, 120, 110, 90]);
baseStatsMapPrelim.set("Gali/Plant", [110, 100, 140, 120, 80, 90]);
baseStatsMapPrelim.set("Gali/Suezo", [90, 130, 160, 120, 100, 110]);
baseStatsMapPrelim.set("Gali/Tiger", [90, 120, 160, 130, 110, 100]);
baseStatsMapPrelim.set("Gali/Worm", [100, 110, 140, 120, 80, 90]);
baseStatsMapPrelim.set("Gali/Zuum", [100, 130, 140, 110, 90, 100]);
baseStatsMapPrelim.set("Ghost/Ghost", [100, 90, 120, 140, 150, 80]);
baseStatsMapPrelim.set("Golem/Arrowhead", [120, 150, 110, 80, 40, 200]);
baseStatsMapPrelim.set("Golem/Bajarl", [110, 160, 120, 90, 70, 130]);
baseStatsMapPrelim.set("Golem/Baku", [140, 210, 70, 50, 40, 150]);
baseStatsMapPrelim.set("Golem/Beaclon", [110, 180, 100, 70, 60, 160]);
baseStatsMapPrelim.set("Golem/Dragon", [100, 220, 130, 90, 60, 150]);
baseStatsMapPrelim.set("Golem/Durahan", [110, 200, 100, 80, 60, 180]);
baseStatsMapPrelim.set("Golem/Gali", [100, 150, 110, 60, 50, 130]);
baseStatsMapPrelim.set("Golem/Golem", [100, 220, 110, 70, 60, 160]);
baseStatsMapPrelim.set("Golem/Hare", [100, 160, 110, 70, 130, 120]);
baseStatsMapPrelim.set("Golem/Henger", [100, 170, 120, 80, 60, 140]);
baseStatsMapPrelim.set("Golem/Jell", [120, 160, 100, 70, 50, 220]);
baseStatsMapPrelim.set("Golem/Joker", [100, 130, 140, 110, 60, 150]);
baseStatsMapPrelim.set("Golem/Metalner", [90, 110, 100, 120, 30, 200]);
baseStatsMapPrelim.set("Golem/Mock", [110, 140, 150, 50, 60, 160]);
baseStatsMapPrelim.set("Golem/Monol", [130, 140, 110, 40, 60, 170]);
baseStatsMapPrelim.set("Golem/Naga", [120, 190, 60, 70, 50, 150]);
baseStatsMapPrelim.set("Golem/Pixie", [90, 150, 140, 80, 70, 120]);
baseStatsMapPrelim.set("Golem/Plant", [140, 100, 110, 90, 60, 130]);
baseStatsMapPrelim.set("Golem/Suezo", [110, 130, 140, 70, 60, 150]);
baseStatsMapPrelim.set("Golem/Tiger", [120, 140, 110, 130, 70, 100]);
baseStatsMapPrelim.set("Golem/Worm", [150, 160, 110, 90, 20, 130]);
baseStatsMapPrelim.set("Golem/Wracky", [90, 130, 100, 60, 70, 120]);
baseStatsMapPrelim.set("Golem/Zilla", [110, 150, 140, 80, 60, 160]);
baseStatsMapPrelim.set("Golem/Zuum", [100, 140, 110, 70, 90, 130]);
baseStatsMapPrelim.set("Hare/Gali", [100, 130, 90, 110, 140, 50]);
baseStatsMapPrelim.set("Hare/Golem", [100, 170, 80, 90, 110, 120]);
baseStatsMapPrelim.set("Hare/Hare", [50, 130, 70, 100, 140, 40]);
baseStatsMapPrelim.set("Hare/Jell", [100, 140, 60, 110, 130, 90]);
baseStatsMapPrelim.set("Hare/Monol", [100, 140, 90, 120, 130, 110]);
baseStatsMapPrelim.set("Hare/Naga", [110, 160, 70, 120, 140, 100]);
baseStatsMapPrelim.set("Hare/Pixie", [80, 130, 100, 120, 180, 50]);
baseStatsMapPrelim.set("Hare/Plant", [120, 110, 90, 100, 140, 70]);
baseStatsMapPrelim.set("Hare/Suezo", [90, 140, 100, 110, 130, 70]);
baseStatsMapPrelim.set("Hare/Tiger", [100, 130, 70, 160, 170, 40]);
baseStatsMapPrelim.set("Hare/Worm", [120, 140, 90, 100, 110, 70]);
baseStatsMapPrelim.set("Hare/Zuum", [100, 130, 70, 110, 140, 80]);
baseStatsMapPrelim.set("Henger/Dragon", [90, 140, 130, 150, 110, 80]);
baseStatsMapPrelim.set("Henger/Golem", [90, 150, 120, 110, 100, 80]);
baseStatsMapPrelim.set("Henger/Henger", [100, 150, 110, 160, 170, 90]);
baseStatsMapPrelim.set("Henger/Joker", [90, 140, 130, 150, 120, 80]);
baseStatsMapPrelim.set("Henger/Metalner", [80, 110, 90, 150, 120, 100]);
baseStatsMapPrelim.set("Henger/Mock", [90, 100, 150, 110, 120, 80]);
baseStatsMapPrelim.set("Henger/Monol", [90, 150, 100, 120, 130, 110]);
baseStatsMapPrelim.set("Henger/Zuum", [90, 130, 110, 140, 150, 80]);
baseStatsMapPrelim.set("Hopper/Bajarl", [60, 100, 110, 170, 120, 50]);
baseStatsMapPrelim.set("Hopper/Dragon", [70, 100, 140, 160, 120, 90]);
baseStatsMapPrelim.set("Hopper/Hopper", [60, 100, 110, 160, 150, 70]);
baseStatsMapPrelim.set("Hopper/Jill", [70, 100, 110, 150, 130, 60]);
baseStatsMapPrelim.set("Hopper/Joker", [70, 90, 110, 160, 110, 60]);
baseStatsMapPrelim.set("Hopper/Kato", [70, 80, 130, 150, 140, 50]);
baseStatsMapPrelim.set("Hopper/Metalner", [70, 80, 90, 160, 100, 110]);
baseStatsMapPrelim.set("Hopper/Mocchi", [60, 100, 110, 160, 120, 70]);
baseStatsMapPrelim.set("Hopper/Mock", [90, 80, 150, 140, 110, 70]);
baseStatsMapPrelim.set("Hopper/Pixie", [70, 90, 160, 170, 130, 60]);
baseStatsMapPrelim.set("Hopper/Suezo", [60, 90, 140, 160, 110, 40]);
baseStatsMapPrelim.set("Hopper/Tiger", [60, 90, 110, 180, 130, 60]);
baseStatsMapPrelim.set("Jell/Gali", [90, 80, 140, 110, 100, 150]);
baseStatsMapPrelim.set("Jell/Golem", [100, 110, 130, 120, 90, 140]);
baseStatsMapPrelim.set("Jell/Hare", [100, 110, 130, 150, 120, 140]);
baseStatsMapPrelim.set("Jell/Jell", [100, 90, 130, 120, 110, 140]);
baseStatsMapPrelim.set("Jell/Monol", [100, 90, 130, 120, 80, 150]);
baseStatsMapPrelim.set("Jell/Naga", [90, 100, 130, 120, 110, 140]);
baseStatsMapPrelim.set("Jell/Pixie", [80, 90, 150, 160, 100, 110]);
baseStatsMapPrelim.set("Jell/Plant", [120, 80, 150, 140, 90, 110]);
baseStatsMapPrelim.set("Jell/Suezo", [100, 90, 150, 130, 80, 120]);
baseStatsMapPrelim.set("Jell/Tiger", [90, 100, 150, 140, 110, 120]);
baseStatsMapPrelim.set("Jell/Worm", [110, 90, 140, 130, 100, 120]);
baseStatsMapPrelim.set("Jell/Zuum", [100, 90, 120, 140, 110, 150]);
baseStatsMapPrelim.set("Jill/Hare", [130, 140, 110, 100, 120, 90]);
baseStatsMapPrelim.set("Jill/Jill", [140, 160, 150, 110, 100, 130]);
baseStatsMapPrelim.set("Jill/Joker", [110, 160, 150, 130, 100, 120]);
baseStatsMapPrelim.set("Jill/Kato", [110, 130, 150, 90, 100, 120]);
baseStatsMapPrelim.set("Jill/Pixie", [90, 130, 150, 120, 110, 100]);
baseStatsMapPrelim.set("Jill/Suezo", [130, 150, 140, 110, 80, 100]);
baseStatsMapPrelim.set("Jill/Tiger", [120, 130, 150, 110, 100, 90]);
baseStatsMapPrelim.set("Joker/Bajarl", [110, 120, 160, 200, 90, 100]);
baseStatsMapPrelim.set("Joker/Dragon", [120, 140, 190, 160, 100, 90]);
baseStatsMapPrelim.set("Joker/Golem", [120, 140, 150, 130, 100, 90]);
baseStatsMapPrelim.set("Joker/Joker", [120, 110, 200, 190, 100, 90]);
baseStatsMapPrelim.set("Joker/Pixie", [100, 110, 190, 200, 120, 90]);
baseStatsMapPrelim.set("Joker/Tiger", [110, 100, 190, 190, 110, 90]);
baseStatsMapPrelim.set("Kato/Dragon", [80, 120, 160, 120, 150, 100]);
baseStatsMapPrelim.set("Kato/Gali", [80, 70, 170, 120, 140, 90]);
baseStatsMapPrelim.set("Kato/Joker", [80, 70, 140, 110, 170, 100]);
baseStatsMapPrelim.set("Kato/Kato", [70, 60, 170, 140, 160, 100]);
baseStatsMapPrelim.set("Kato/Mocchi", [70, 80, 170, 140, 150, 90]);
baseStatsMapPrelim.set("Kato/Suezo", [70, 80, 170, 140, 150, 90]);
baseStatsMapPrelim.set("Kato/Tiger", [70, 60, 170, 140, 160, 100]);
baseStatsMapPrelim.set("Metalner/Metalner", [50, 20, 10, 160, 30, 170]);
baseStatsMapPrelim.set("Metalner/Pixie", [60, 50, 100, 160, 80, 110]);
baseStatsMapPrelim.set("Metalner/Suezo", [70, 90, 100, 170, 60, 160]);
baseStatsMapPrelim.set("Mew/Hare", [130, 100, 80, 140, 150, 90]);
baseStatsMapPrelim.set("Mew/Jell", [130, 80, 100, 140, 110, 90]);
baseStatsMapPrelim.set("Mew/Mew", [130, 80, 70, 120, 140, 90]);
baseStatsMapPrelim.set("Mew/Pixie", [110, 80, 100, 130, 140, 90]);
baseStatsMapPrelim.set("Mew/Tiger", [100, 80, 120, 130, 140, 90]);
baseStatsMapPrelim.set("Mocchi/Dragon", [100, 110, 140, 150, 120, 160]);
baseStatsMapPrelim.set("Mocchi/Durahan", [100, 110, 120, 150, 130, 140]);
baseStatsMapPrelim.set("Mocchi/Jell", [110, 100, 130, 140, 120, 150]);
baseStatsMapPrelim.set("Mocchi/Joker", [100, 110, 140, 150, 130, 110]);
baseStatsMapPrelim.set("Mocchi/Kato", [100, 80, 150, 140, 130, 120]);
baseStatsMapPrelim.set("Mocchi/Mocchi", [110, 100, 120, 140, 150, 130]);
baseStatsMapPrelim.set("Mocchi/Pixie", [100, 110, 120, 140, 150, 130]);
baseStatsMapPrelim.set("Mocchi/Tiger", [110, 100, 120, 140, 150, 130]);
baseStatsMapPrelim.set("Mock/Joker", [200, 80, 170, 120, 90, 60]);
baseStatsMapPrelim.set("Mock/Mock", [200, 70, 140, 50, 60, 40]);
baseStatsMapPrelim.set("Monol/Gali", [90, 110, 130, 100, 30, 150]);
baseStatsMapPrelim.set("Monol/Golem", [90, 130, 120, 100, 10, 200]);
baseStatsMapPrelim.set("Monol/Hare", [80, 140, 100, 90, 110, 120]);
baseStatsMapPrelim.set("Monol/Jell", [80, 110, 120, 100, 40, 220]);
baseStatsMapPrelim.set("Monol/Monol", [110, 130, 140, 100, 10, 220]);
baseStatsMapPrelim.set("Monol/Naga", [90, 120, 100, 130, 60, 160]);
baseStatsMapPrelim.set("Monol/Plant", [100, 100, 130, 90, 40, 120]);
baseStatsMapPrelim.set("Monol/Pixie", [90, 130, 150, 100, 50, 120]);
baseStatsMapPrelim.set("Monol/Suezo", [100, 110, 150, 120, 60, 140]);
baseStatsMapPrelim.set("Monol/Tiger", [100, 110, 120, 100, 40, 130]);
baseStatsMapPrelim.set("Monol/Worm", [100, 120, 130, 90, 50, 160]);
baseStatsMapPrelim.set("Monol/Zuum", [90, 110, 120, 100, 30, 150]);
baseStatsMapPrelim.set("Naga/Gali", [120, 150, 110, 140, 70, 130]);
baseStatsMapPrelim.set("Naga/Golem", [130, 150, 100, 120, 90, 140]);
baseStatsMapPrelim.set("Naga/Hare", [130, 160, 90, 150, 120, 110]);
baseStatsMapPrelim.set("Naga/Jell", [110, 120, 90, 150, 100, 130]);
baseStatsMapPrelim.set("Naga/Monol", [120, 140, 90, 110, 100, 150]);
baseStatsMapPrelim.set("Naga/Naga", [120, 160, 100, 150, 110, 130]);
baseStatsMapPrelim.set("Naga/Pixie", [100, 130, 110, 140, 120, 90]);
baseStatsMapPrelim.set("Naga/Plant", [160, 120, 100, 150, 90, 70]);
baseStatsMapPrelim.set("Naga/Suezo", [120, 140, 110, 130, 80, 100]);
baseStatsMapPrelim.set("Naga/Tiger", [120, 130, 80, 160, 110, 100]);
baseStatsMapPrelim.set("Naga/Worm", [120, 130, 70, 150, 90, 110]);
baseStatsMapPrelim.set("Naga/Zuum", [110, 150, 60, 140, 110, 120]);
baseStatsMapPrelim.set("Niton/Bajarl", [80, 120, 60, 130, 90, 110]);
baseStatsMapPrelim.set("Niton/Durahan", [90, 130, 80, 70, 140, 100]);
baseStatsMapPrelim.set("Niton/Golem", [100, 120, 50, 60, 140, 80]);
baseStatsMapPrelim.set("Niton/Jell", [70, 60, 120, 110, 90, 140]);
baseStatsMapPrelim.set("Niton/Kato", [90, 60, 110, 80, 120, 130]);
baseStatsMapPrelim.set("Niton/Metalner", [80, 50, 90, 120, 70, 150]);
baseStatsMapPrelim.set("Niton/Mock", [80, 60, 120, 70, 40, 130]);
baseStatsMapPrelim.set("Niton/Niton", [90, 40, 30, 70, 50, 160]);
baseStatsMapPrelim.set("Phoenix/Phoenix", [170, 150, 190, 140, 160, 110]);
baseStatsMapPrelim.set("Pixie/Bajarl", [70, 110, 130, 150, 140, 60]);
baseStatsMapPrelim.set("Pixie/Centaur", [70, 80, 170, 150, 100, 60]);
baseStatsMapPrelim.set("Pixie/Dragon", [90, 110, 190, 140, 120, 80]);
baseStatsMapPrelim.set("Pixie/Durahan", [80, 110, 140, 150, 100, 130]);
baseStatsMapPrelim.set("Pixie/Gali", [50, 80, 170, 150, 110, 60]);
baseStatsMapPrelim.set("Pixie/Golem", [80, 110, 150, 120, 100, 130]);
baseStatsMapPrelim.set("Pixie/Hare", [70, 100, 110, 150, 140, 60]);
baseStatsMapPrelim.set("Pixie/Jell", [50, 80, 170, 150, 110, 60]);
baseStatsMapPrelim.set("Pixie/Jill", [70, 110, 170, 100, 120, 80]);
baseStatsMapPrelim.set("Pixie/Joker", [70, 80, 170, 150, 110, 60]);
baseStatsMapPrelim.set("Pixie/Kato", [50, 80, 170, 150, 140, 60]);
baseStatsMapPrelim.set("Pixie/Metalner", [50, 80, 100, 150, 110, 130]);
baseStatsMapPrelim.set("Pixie/Mock", [150, 80, 170, 100, 110, 60]);
baseStatsMapPrelim.set("Pixie/Monol", [50, 80, 140, 120, 110, 130]);
baseStatsMapPrelim.set("Pixie/Naga", [80, 120, 110, 150, 130, 80]);
baseStatsMapPrelim.set("Pixie/Pixie", [50, 80, 170, 150, 140, 60]);
baseStatsMapPrelim.set("Pixie/Plant", [100, 80, 140, 150, 110, 60]);
baseStatsMapPrelim.set("Pixie/Suezo", [50, 80, 170, 150, 110, 60]);
baseStatsMapPrelim.set("Pixie/Tiger", [50, 80, 170, 150, 140, 60]);
baseStatsMapPrelim.set("Pixie/Worm", [100, 80, 140, 150, 110, 60]);
baseStatsMapPrelim.set("Pixie/Wracky", [50, 80, 170, 100, 140, 60]);
baseStatsMapPrelim.set("Pixie/Zuum", [70, 80, 130, 150, 140, 90]);
baseStatsMapPrelim.set("Plant/Gali", [150, 60, 140, 110, 100, 70]);
baseStatsMapPrelim.set("Plant/Golem", [160, 120, 110, 80, 70, 130]);
baseStatsMapPrelim.set("Plant/Hare", [160, 130, 100, 120, 110, 60]);
baseStatsMapPrelim.set("Plant/Jell", [140, 50, 120, 110, 100, 80]);
baseStatsMapPrelim.set("Plant/Monol", [150, 90, 130, 120, 100, 110]);
baseStatsMapPrelim.set("Plant/Naga", [130, 70, 120, 110, 100, 90]);
baseStatsMapPrelim.set("Plant/Pixie", [120, 40, 140, 110, 100, 60]);
baseStatsMapPrelim.set("Plant/Plant", [160, 40, 120, 110, 100, 70]);
baseStatsMapPrelim.set("Plant/Suezo", [140, 80, 150, 110, 90, 50]);
baseStatsMapPrelim.set("Plant/Tiger", [140, 70, 120, 110, 90, 50]);
baseStatsMapPrelim.set("Plant/Worm", [170, 80, 110, 100, 90, 50]);
baseStatsMapPrelim.set("Plant/Zuum", [150, 80, 120, 110, 100, 90]);
baseStatsMapPrelim.set("Suezo/Gali", [80, 120, 150, 130, 90, 100]);
baseStatsMapPrelim.set("Suezo/Golem", [80, 120, 170, 130, 90, 100]);
baseStatsMapPrelim.set("Suezo/Hare", [80, 150, 130, 140, 110, 90]);
baseStatsMapPrelim.set("Suezo/Jell", [100, 110, 150, 130, 90, 120]);
baseStatsMapPrelim.set("Suezo/Monol", [80, 120, 140, 100, 90, 110]);
baseStatsMapPrelim.set("Suezo/Naga", [100, 130, 110, 150, 80, 90]);
baseStatsMapPrelim.set("Suezo/Pixie", [80, 120, 170, 130, 100, 90]);
baseStatsMapPrelim.set("Suezo/Plant", [120, 100, 150, 130, 80, 90]);
baseStatsMapPrelim.set("Suezo/Suezo", [80, 120, 170, 130, 90, 100]);
baseStatsMapPrelim.set("Suezo/Tiger", [80, 120, 160, 130, 110, 100]);
baseStatsMapPrelim.set("Suezo/Worm", [110, 120, 140, 130, 90, 100]);
baseStatsMapPrelim.set("Suezo/Zuum", [80, 120, 150, 130, 90, 100]);
baseStatsMapPrelim.set("Tiger/Gali", [80, 90, 130, 150, 140, 70]);
baseStatsMapPrelim.set("Tiger/Golem", [80, 100, 140, 110, 130, 120]);
baseStatsMapPrelim.set("Tiger/Hare", [80, 100, 120, 150, 140, 70]);
baseStatsMapPrelim.set("Tiger/Jell", [70, 90, 150, 160, 120, 80]);
baseStatsMapPrelim.set("Tiger/Monol", [100, 90, 140, 150, 130, 120]);
baseStatsMapPrelim.set("Tiger/Naga", [90, 120, 110, 190, 130, 80]);
baseStatsMapPrelim.set("Tiger/Pixie", [90, 80, 140, 170, 150, 60]);
baseStatsMapPrelim.set("Tiger/Plant", [100, 90, 120, 150, 110, 50]);
baseStatsMapPrelim.set("Tiger/Suezo", [90, 80, 130, 170, 100, 60]);
baseStatsMapPrelim.set("Tiger/Tiger", [80, 90, 130, 160, 140, 70]);
baseStatsMapPrelim.set("Tiger/Worm", [80, 90, 150, 120, 110, 50]);
baseStatsMapPrelim.set("Tiger/Zuum", [80, 90, 120, 160, 140, 100]);
baseStatsMapPrelim.set("Undine/Joker", [70, 90, 150, 170, 120, 60]);
baseStatsMapPrelim.set("Undine/Undine", [50, 10, 150, 110, 100, 60]);
baseStatsMapPrelim.set("Worm/Gali", [150, 100, 130, 110, 60, 80]);
baseStatsMapPrelim.set("Worm/Golem", [140, 150, 120, 100, 40, 110]);
baseStatsMapPrelim.set("Worm/Hare", [130, 140, 80, 120, 110, 90]);
baseStatsMapPrelim.set("Worm/Jell", [140, 110, 120, 130, 40, 100]);
baseStatsMapPrelim.set("Worm/Monol", [150, 120, 110, 130, 70, 90]);
baseStatsMapPrelim.set("Worm/Naga", [160, 120, 100, 110, 50, 90]);
baseStatsMapPrelim.set("Worm/Pixie", [130, 110, 140, 120, 80, 90]);
baseStatsMapPrelim.set("Worm/Plant", [170, 100, 120, 130, 60, 90]);
baseStatsMapPrelim.set("Worm/Suezo", [150, 120, 130, 110, 60, 90]);
baseStatsMapPrelim.set("Worm/Tiger", [150, 100, 110, 130, 90, 80]);
baseStatsMapPrelim.set("Worm/Worm", [180, 100, 110, 120, 60, 90]);
baseStatsMapPrelim.set("Worm/Zuum", [140, 100, 120, 110, 90, 80]);
baseStatsMapPrelim.set("Wracky/Bajarl", [80, 70, 120, 90, 150, 50]);
baseStatsMapPrelim.set("Wracky/Dragon", [90, 100, 150, 80, 110, 70]);
baseStatsMapPrelim.set("Wracky/Durahan", [50, 40, 140, 60, 120, 100]);
baseStatsMapPrelim.set("Wracky/Golem", [80, 120, 150, 60, 100, 110]);
baseStatsMapPrelim.set("Wracky/Henger", [40, 70, 140, 90, 150, 50]);
baseStatsMapPrelim.set("Wracky/Joker", [80, 70, 130, 100, 120, 50]);
baseStatsMapPrelim.set("Wracky/Metalner", [60, 50, 120, 100, 130, 110]);
baseStatsMapPrelim.set("Wracky/Mock", [60, 50, 150, 40, 120, 30]);
baseStatsMapPrelim.set("Wracky/Pixie", [60, 30, 140, 90, 150, 50]);
baseStatsMapPrelim.set("Wracky/Wracky", [20, 10, 150, 40, 160, 30]);
baseStatsMapPrelim.set("Zilla/Jell", [100, 150, 120, 80, 90, 110]);
baseStatsMapPrelim.set("Zilla/Pixie", [120, 150, 110, 70, 100, 90]);
baseStatsMapPrelim.set("Zilla/Tiger", [140, 160, 130, 110, 120, 100]);
baseStatsMapPrelim.set("Zilla/Zilla", [150, 180, 80, 50, 60, 100]);
baseStatsMapPrelim.set("Zuum/Arrowhead", [130, 120, 80, 150, 110, 140]);
baseStatsMapPrelim.set("Zuum/Bajarl", [130, 120, 80, 150, 100, 110]);
baseStatsMapPrelim.set("Zuum/Baku", [130, 120, 80, 110, 90, 100]);
baseStatsMapPrelim.set("Zuum/Dragon", [90, 120, 110, 140, 100, 80]);
baseStatsMapPrelim.set("Zuum/Gali", [140, 130, 100, 150, 110, 120]);
baseStatsMapPrelim.set("Zuum/Golem", [110, 140, 80, 120, 100, 130]);
baseStatsMapPrelim.set("Zuum/Hare", [120, 140, 80, 150, 130, 100]);
baseStatsMapPrelim.set("Zuum/Jell", [120, 130, 100, 140, 110, 140]);
baseStatsMapPrelim.set("Zuum/Joker", [120, 100, 90, 140, 130, 110]);
baseStatsMapPrelim.set("Zuum/Kato", [120, 90, 130, 140, 100, 110]);
baseStatsMapPrelim.set("Zuum/Mock", [90, 130, 100, 120, 140, 110]);
baseStatsMapPrelim.set("Zuum/Monol", [110, 100, 80, 120, 90, 150]);
baseStatsMapPrelim.set("Zuum/Naga", [130, 100, 60, 150, 120, 110]);
baseStatsMapPrelim.set("Zuum/Pixie", [70, 110, 120, 140, 100, 90]);
baseStatsMapPrelim.set("Zuum/Plant", [130, 90, 80, 140, 120, 100]);
baseStatsMapPrelim.set("Zuum/Suezo", [140, 110, 100, 150, 120, 130]);
baseStatsMapPrelim.set("Zuum/Tiger", [130, 120, 100, 150, 110, 90]);
baseStatsMapPrelim.set("Zuum/Worm", [150, 110, 80, 130, 70, 120]);
baseStatsMapPrelim.set("Zuum/Zuum", [130, 120, 80, 140, 100, 110]);

var baseStatsList = Array.from(baseStatsMapPrelim.entries());
var baseStatsMap = new Map();

baseStatsList.forEach(function (element) {
  baseStatsMap.set(element[0], [
    { label: "LIF", value: element[1][0] },
    { label: "POW", value: element[1][1] },
    { label: "INT", value: element[1][2] },
    { label: "SKL", value: element[1][3] },
    { label: "SPD", value: element[1][4] },
    { label: "DEF", value: element[1][5] },
  ]);
});

var baseStatsMapPrelimHard = new Map();

baseStatsMapPrelimHard.set("Ape/Ape", [150, 160, 20, 120, 100, 140]);
baseStatsMapPrelimHard.set("Ape/Gali", [130, 150, 120, 110, 100, 140]);
baseStatsMapPrelimHard.set("Ape/Golem", [160, 180, 70, 100, 80, 150]);
baseStatsMapPrelimHard.set("Ape/Hare", [160, 140, 60, 110, 150, 120]);
baseStatsMapPrelimHard.set("Ape/Plant", [140, 120, 90, 110, 100, 130]);
baseStatsMapPrelimHard.set("Arrowhead/Arrowhead", [120, 80, 70, 30, 40, 170]);
baseStatsMapPrelimHard.set("Arrowhead/Durahan", [130, 120, 80, 110, 100, 170]);
baseStatsMapPrelimHard.set("Arrowhead/Golem", [120, 150, 70, 50, 60, 190]);
baseStatsMapPrelimHard.set("Arrowhead/Henger", [120, 100, 60, 110, 90, 130]);
baseStatsMapPrelimHard.set("Arrowhead/Joker", [100, 110, 120, 140, 70, 150]);
baseStatsMapPrelimHard.set("Arrowhead/Mock", [90, 110, 130, 100, 60, 150]);
baseStatsMapPrelimHard.set("Arrowhead/Suezo", [100, 120, 130, 110, 70, 140]);
baseStatsMapPrelimHard.set("Bajarl/Bajarl", [100, 130, 90, 120, 110, 80]);
baseStatsMapPrelimHard.set("Bajarl/Joker", [100, 150, 120, 140, 110, 90]);
baseStatsMapPrelimHard.set("Baku/Baku", [180, 130, 50, 70, 60, 150]);
baseStatsMapPrelimHard.set("Baku/Dragon", [150, 130, 100, 80, 60, 120]);
baseStatsMapPrelimHard.set("Baku/Durahan", [150, 140, 70, 80, 50, 160]);
baseStatsMapPrelimHard.set("Baku/Golem", [160, 130, 70, 90, 60, 150]);
baseStatsMapPrelimHard.set("Baku/Hare", [150, 130, 50, 90, 100, 80]);
baseStatsMapPrelimHard.set("Baku/Jell", [150, 120, 90, 100, 70, 130]);
baseStatsMapPrelimHard.set("Baku/Joker", [160, 140, 100, 110, 70, 120]);
baseStatsMapPrelimHard.set("Baku/Kato", [160, 110, 120, 70, 100, 130]);
baseStatsMapPrelimHard.set("Baku/Tiger", [150, 120, 50, 100, 70, 90]);
baseStatsMapPrelimHard.set("Beaclon/Bajarl", [120, 150, 50, 100, 90, 110]);
baseStatsMapPrelimHard.set("Beaclon/Beaclon", [120, 150, 50, 70, 90, 140]);
baseStatsMapPrelimHard.set("Beaclon/Dragon", [120, 150, 110, 70, 90, 140]);
baseStatsMapPrelimHard.set("Beaclon/Ducken", [90, 150, 70, 100, 120, 110]);
baseStatsMapPrelimHard.set("Beaclon/Durahan", [120, 150, 80, 70, 90, 140]);
baseStatsMapPrelimHard.set("Beaclon/Golem", [120, 150, 80, 70, 90, 140]);
baseStatsMapPrelimHard.set("Beaclon/Henger", [100, 150, 80, 110, 120, 130]);
baseStatsMapPrelimHard.set("Beaclon/Joker", [120, 150, 130, 100, 90, 110]);
baseStatsMapPrelimHard.set("Beaclon/Tiger", [100, 110, 70, 130, 90, 120]);
baseStatsMapPrelimHard.set("Centaur/Arrowhead", [90, 100, 110, 140, 150, 120]);
baseStatsMapPrelimHard.set("Centaur/Bajarl", [90, 100, 110, 160, 150, 80]);
baseStatsMapPrelimHard.set("Centaur/Centaur", [90, 100, 140, 160, 150, 80]);
baseStatsMapPrelimHard.set("Centaur/Dragon", [90, 130, 140, 120, 150, 70]);
baseStatsMapPrelimHard.set("Centaur/Durahan", [90, 100, 140, 130, 150, 120]);
baseStatsMapPrelimHard.set("Centaur/Golem", [90, 130, 140, 100, 150, 110]);
baseStatsMapPrelimHard.set("Centaur/Joker", [90, 100, 140, 160, 150, 80]);
baseStatsMapPrelimHard.set("Centaur/Pixie", [70, 100, 140, 160, 110, 80]);
baseStatsMapPrelimHard.set("Centaur/Tiger", [90, 100, 140, 160, 170, 80]);
baseStatsMapPrelimHard.set("ColorPandora/ColorPandora", [
  170,
  80,
  40,
  110,
  70,
  60,
]);
baseStatsMapPrelimHard.set("ColorPandora/Jell", [140, 70, 90, 110, 50, 120]);
baseStatsMapPrelimHard.set("ColorPandora/Pixie", [120, 30, 130, 110, 100, 20]);
baseStatsMapPrelimHard.set("Dragon/Arrowhead", [120, 150, 140, 110, 50, 180]);
baseStatsMapPrelimHard.set("Dragon/Bajarl", [140, 180, 130, 120, 80, 100]);
baseStatsMapPrelimHard.set("Dragon/Beaclon", [140, 170, 100, 130, 50, 150]);
baseStatsMapPrelimHard.set("Dragon/Dragon", [130, 170, 140, 120, 40, 150]);
baseStatsMapPrelimHard.set("Dragon/Durahan", [100, 170, 140, 120, 90, 130]);
baseStatsMapPrelimHard.set("Dragon/Gali", [100, 160, 170, 110, 90, 130]);
baseStatsMapPrelimHard.set("Dragon/Golem", [140, 240, 130, 80, 10, 150]);
baseStatsMapPrelimHard.set("Dragon/Henger", [120, 170, 150, 130, 90, 110]);
baseStatsMapPrelimHard.set("Dragon/Joker", [120, 130, 210, 150, 60, 80]);
baseStatsMapPrelimHard.set("Dragon/Kato", [90, 150, 170, 120, 140, 80]);
baseStatsMapPrelimHard.set("Dragon/Metalner", [130, 110, 90, 160, 20, 170]);
baseStatsMapPrelimHard.set("Dragon/Monol", [180, 130, 140, 120, 10, 190]);
baseStatsMapPrelimHard.set("Dragon/Pixie", [100, 120, 200, 110, 80, 90]);
baseStatsMapPrelimHard.set("Dragon/Tiger", [110, 140, 160, 130, 80, 100]);
baseStatsMapPrelimHard.set("Ducken/Ducken", [70, 80, 130, 140, 100, 50]);
baseStatsMapPrelimHard.set("Ducken/Golem", [70, 150, 120, 130, 100, 110]);
baseStatsMapPrelimHard.set("Ducken/Suezo", [80, 100, 130, 150, 140, 50]);
baseStatsMapPrelimHard.set("Durahan/Arrowhead", [120, 140, 90, 110, 50, 160]);
baseStatsMapPrelimHard.set("Durahan/Beaclon", [100, 150, 80, 110, 30, 180]);
baseStatsMapPrelimHard.set("Durahan/Dragon", [100, 130, 150, 110, 70, 140]);
baseStatsMapPrelimHard.set("Durahan/Durahan", [100, 150, 80, 110, 70, 180]);
baseStatsMapPrelimHard.set("Durahan/Golem", [120, 150, 100, 90, 60, 170]);
baseStatsMapPrelimHard.set("Durahan/Joker", [100, 120, 150, 140, 80, 130]);
baseStatsMapPrelimHard.set("Durahan/Metalner", [90, 120, 80, 140, 70, 160]);
baseStatsMapPrelimHard.set("Durahan/Mock", [90, 120, 130, 110, 70, 150]);
baseStatsMapPrelimHard.set("Durahan/Phoenix", [100, 120, 130, 110, 90, 150]);
baseStatsMapPrelimHard.set("Durahan/Pixie", [90, 110, 150, 130, 100, 120]);
baseStatsMapPrelimHard.set("Durahan/Tiger", [130, 110, 120, 140, 90, 100]);
baseStatsMapPrelimHard.set("Gaboo/Gaboo", [190, 130, 30, 80, 150, 50]);
baseStatsMapPrelimHard.set("Gaboo/Jell", [140, 130, 70, 90, 120, 80]);
baseStatsMapPrelimHard.set("Gaboo/Joker", [150, 140, 130, 110, 100, 40]);
baseStatsMapPrelimHard.set("Gaboo/Tiger", [160, 120, 60, 100, 150, 50]);
baseStatsMapPrelimHard.set("Gali/Gali", [100, 150, 160, 120, 90, 130]);
baseStatsMapPrelimHard.set("Gali/Golem", [100, 190, 120, 110, 90, 150]);
baseStatsMapPrelimHard.set("Gali/Hare", [100, 170, 110, 120, 130, 90]);
baseStatsMapPrelimHard.set("Gali/Jell", [100, 120, 160, 130, 80, 150]);
baseStatsMapPrelimHard.set("Gali/Monol", [120, 130, 150, 110, 70, 180]);
baseStatsMapPrelimHard.set("Gali/Naga", [90, 160, 120, 140, 100, 110]);
baseStatsMapPrelimHard.set("Gali/Pixie", [90, 110, 170, 120, 140, 80]);
baseStatsMapPrelimHard.set("Gali/Plant", [140, 100, 150, 120, 90, 80]);
baseStatsMapPrelimHard.set("Gali/Suezo", [80, 120, 200, 140, 90, 110]);
baseStatsMapPrelimHard.set("Gali/Tiger", [100, 110, 160, 140, 120, 80]);
baseStatsMapPrelimHard.set("Gali/Worm", [130, 150, 140, 120, 90, 100]);
baseStatsMapPrelimHard.set("Gali/Zuum", [100, 150, 140, 130, 90, 110]);
baseStatsMapPrelimHard.set("Ghost/Ghost", [100, 110, 120, 140, 150, 60]);
baseStatsMapPrelimHard.set("Golem/Arrowhead", [120, 150, 110, 90, 30, 210]);
baseStatsMapPrelimHard.set("Golem/Bajarl", [120, 160, 110, 100, 70, 130]);
baseStatsMapPrelimHard.set("Golem/Baku", [140, 210, 70, 50, 40, 150]);
baseStatsMapPrelimHard.set("Golem/Beaclon", [110, 180, 100, 70, 60, 160]);
baseStatsMapPrelimHard.set("Golem/Dragon", [100, 240, 120, 90, 50, 160]);
baseStatsMapPrelimHard.set("Golem/Durahan", [110, 200, 100, 80, 60, 180]);
baseStatsMapPrelimHard.set("Golem/Gali", [120, 160, 140, 90, 60, 150]);
baseStatsMapPrelimHard.set("Golem/Golem", [100, 220, 110, 70, 60, 160]);
baseStatsMapPrelimHard.set("Golem/Hare", [100, 160, 110, 70, 130, 120]);
baseStatsMapPrelimHard.set("Golem/Henger", [120, 170, 100, 90, 70, 140]);
baseStatsMapPrelimHard.set("Golem/Jell", [120, 160, 100, 70, 50, 220]);
baseStatsMapPrelimHard.set("Golem/Joker", [110, 130, 140, 120, 60, 150]);
baseStatsMapPrelimHard.set("Golem/Metalner", [100, 130, 80, 120, 10, 200]);
baseStatsMapPrelimHard.set("Golem/Mock", [170, 150, 130, 60, 30, 160]);
baseStatsMapPrelimHard.set("Golem/Monol", [160, 150, 110, 60, 40, 200]);
baseStatsMapPrelimHard.set("Golem/Naga", [120, 200, 60, 100, 50, 140]);
baseStatsMapPrelimHard.set("Golem/Pixie", [90, 150, 140, 80, 70, 120]);
baseStatsMapPrelimHard.set("Golem/Plant", [160, 130, 100, 90, 60, 110]);
baseStatsMapPrelimHard.set("Golem/Suezo", [110, 130, 180, 90, 60, 140]);
baseStatsMapPrelimHard.set("Golem/Tiger", [120, 140, 110, 130, 70, 100]);
baseStatsMapPrelimHard.set("Golem/Worm", [170, 160, 110, 90, 20, 130]);
baseStatsMapPrelimHard.set("Golem/Wracky", [100, 140, 90, 60, 80, 130]);
baseStatsMapPrelimHard.set("Golem/Zilla", [150, 200, 90, 80, 50, 130]);
baseStatsMapPrelimHard.set("Golem/Zuum", [120, 150, 110, 100, 70, 140]);
baseStatsMapPrelimHard.set("Hare/Gali", [110, 140, 100, 120, 130, 80]);
baseStatsMapPrelimHard.set("Hare/Golem", [100, 200, 50, 90, 120, 110]);
baseStatsMapPrelimHard.set("Hare/Hare", [90, 140, 70, 110, 150, 60]);
baseStatsMapPrelimHard.set("Hare/Jell", [100, 140, 70, 120, 130, 90]);
baseStatsMapPrelimHard.set("Hare/Monol", [140, 150, 80, 110, 90, 130]);
baseStatsMapPrelimHard.set("Hare/Naga", [110, 160, 70, 120, 140, 100]);
baseStatsMapPrelimHard.set("Hare/Pixie", [80, 130, 100, 120, 180, 50]);
baseStatsMapPrelimHard.set("Hare/Plant", [140, 100, 90, 110, 120, 70]);
baseStatsMapPrelimHard.set("Hare/Suezo", [90, 130, 110, 120, 140, 60]);
baseStatsMapPrelimHard.set("Hare/Tiger", [100, 130, 70, 160, 170, 40]);
baseStatsMapPrelimHard.set("Hare/Worm", [130, 140, 90, 100, 120, 70]);
baseStatsMapPrelimHard.set("Hare/Zuum", [100, 140, 70, 110, 150, 80]);
baseStatsMapPrelimHard.set("Henger/Dragon", [100, 140, 130, 150, 120, 90]);
baseStatsMapPrelimHard.set("Henger/Golem", [80, 200, 120, 110, 100, 90]);
baseStatsMapPrelimHard.set("Henger/Henger", [100, 150, 110, 160, 170, 90]);
baseStatsMapPrelimHard.set("Henger/Joker", [90, 130, 160, 150, 120, 70]);
baseStatsMapPrelimHard.set("Henger/Metalner", [80, 110, 90, 150, 120, 100]);
baseStatsMapPrelimHard.set("Henger/Mock", [90, 100, 150, 110, 120, 80]);
baseStatsMapPrelimHard.set("Henger/Monol", [130, 150, 100, 110, 90, 120]);
baseStatsMapPrelimHard.set("Henger/Zuum", [90, 130, 110, 140, 150, 80]);
baseStatsMapPrelimHard.set("Hopper/Bajarl", [60, 100, 110, 170, 120, 50]);
baseStatsMapPrelimHard.set("Hopper/Dragon", [70, 100, 140, 160, 120, 90]);
baseStatsMapPrelimHard.set("Hopper/Hopper", [60, 100, 110, 160, 150, 70]);
baseStatsMapPrelimHard.set("Hopper/Jill", [70, 100, 110, 150, 130, 60]);
baseStatsMapPrelimHard.set("Hopper/Joker", [70, 90, 110, 160, 110, 60]);
baseStatsMapPrelimHard.set("Hopper/Kato", [70, 80, 130, 150, 140, 50]);
baseStatsMapPrelimHard.set("Hopper/Metalner", [70, 80, 90, 160, 100, 110]);
baseStatsMapPrelimHard.set("Hopper/Mocchi", [60, 100, 110, 160, 120, 70]);
baseStatsMapPrelimHard.set("Hopper/Mock", [90, 80, 150, 140, 110, 70]);
baseStatsMapPrelimHard.set("Hopper/Pixie", [70, 90, 160, 170, 130, 60]);
baseStatsMapPrelimHard.set("Hopper/Suezo", [60, 90, 140, 160, 110, 40]);
baseStatsMapPrelimHard.set("Hopper/Tiger", [60, 90, 110, 180, 130, 60]);
baseStatsMapPrelimHard.set("Jell/Gali", [110, 100, 150, 120, 80, 140]);
baseStatsMapPrelimHard.set("Jell/Golem", [110, 130, 140, 120, 30, 170]);
baseStatsMapPrelimHard.set("Jell/Hare", [110, 140, 100, 150, 120, 130]);
baseStatsMapPrelimHard.set("Jell/Jell", [110, 100, 130, 120, 90, 140]);
baseStatsMapPrelimHard.set("Jell/Monol", [160, 100, 130, 120, 50, 150]);
baseStatsMapPrelimHard.set("Jell/Naga", [110, 120, 100, 140, 90, 130]);
baseStatsMapPrelimHard.set("Jell/Pixie", [90, 80, 160, 150, 110, 100]);
baseStatsMapPrelimHard.set("Jell/Plant", [150, 80, 120, 140, 90, 110]);
baseStatsMapPrelimHard.set("Jell/Suezo", [100, 90, 180, 130, 80, 110]);
baseStatsMapPrelimHard.set("Jell/Tiger", [100, 90, 150, 140, 110, 120]);
baseStatsMapPrelimHard.set("Jell/Worm", [150, 100, 140, 120, 80, 110]);
baseStatsMapPrelimHard.set("Jell/Zuum", [100, 90, 120, 140, 110, 150]);
baseStatsMapPrelimHard.set("Jill/Hare", [130, 140, 110, 100, 120, 90]);
baseStatsMapPrelimHard.set("Jill/Jill", [140, 160, 150, 110, 100, 130]);
baseStatsMapPrelimHard.set("Jill/Joker", [110, 160, 150, 130, 100, 120]);
baseStatsMapPrelimHard.set("Jill/Kato", [110, 130, 150, 90, 100, 120]);
baseStatsMapPrelimHard.set("Jill/Pixie", [90, 130, 150, 120, 110, 100]);
baseStatsMapPrelimHard.set("Jill/Suezo", [130, 150, 140, 110, 80, 100]);
baseStatsMapPrelimHard.set("Jill/Tiger", [120, 130, 150, 110, 100, 90]);
baseStatsMapPrelimHard.set("Joker/Bajarl", [110, 130, 160, 200, 90, 100]);
baseStatsMapPrelimHard.set("Joker/Dragon", [130, 150, 210, 160, 100, 120]);
baseStatsMapPrelimHard.set("Joker/Golem", [120, 190, 150, 130, 100, 110]);
baseStatsMapPrelimHard.set("Joker/Joker", [120, 110, 200, 190, 100, 90]);
baseStatsMapPrelimHard.set("Joker/Pixie", [100, 110, 190, 200, 120, 90]);
baseStatsMapPrelimHard.set("Joker/Tiger", [100, 110, 180, 200, 120, 80]);
baseStatsMapPrelimHard.set("Kato/Dragon", [80, 130, 160, 120, 150, 110]);
baseStatsMapPrelimHard.set("Kato/Gali", [80, 70, 170, 120, 140, 90]);
baseStatsMapPrelimHard.set("Kato/Joker", [80, 70, 140, 110, 170, 100]);
baseStatsMapPrelimHard.set("Kato/Kato", [60, 100, 170, 140, 160, 70]);
baseStatsMapPrelimHard.set("Kato/Mocchi", [70, 80, 170, 140, 150, 90]);
baseStatsMapPrelimHard.set("Kato/Suezo", [70, 80, 170, 140, 150, 90]);
baseStatsMapPrelimHard.set("Kato/Tiger", [70, 60, 170, 140, 160, 100]);
baseStatsMapPrelimHard.set("Metalner/Metalner", [80, 50, 30, 160, 40, 170]);
baseStatsMapPrelimHard.set("Metalner/Pixie", [60, 50, 100, 160, 80, 110]);
baseStatsMapPrelimHard.set("Metalner/Suezo", [70, 90, 100, 170, 60, 160]);
baseStatsMapPrelimHard.set("Mew/Hare", [130, 100, 80, 140, 150, 90]);
baseStatsMapPrelimHard.set("Mew/Jell", [130, 80, 100, 140, 110, 90]);
baseStatsMapPrelimHard.set("Mew/Mew", [130, 80, 70, 120, 140, 90]);
baseStatsMapPrelimHard.set("Mew/Pixie", [110, 80, 100, 130, 140, 90]);
baseStatsMapPrelimHard.set("Mew/Tiger", [100, 80, 120, 130, 140, 90]);
baseStatsMapPrelimHard.set("Mocchi/Dragon", [110, 130, 150, 140, 70, 180]);
baseStatsMapPrelimHard.set("Mocchi/Durahan", [100, 120, 110, 140, 80, 180]);
baseStatsMapPrelimHard.set("Mocchi/Jell", [100, 110, 130, 140, 90, 160]);
baseStatsMapPrelimHard.set("Mocchi/Joker", [80, 110, 150, 160, 100, 140]);
baseStatsMapPrelimHard.set("Mocchi/Kato", [80, 100, 170, 140, 120, 130]);
baseStatsMapPrelimHard.set("Mocchi/Mocchi", [100, 110, 120, 130, 90, 160]);
baseStatsMapPrelimHard.set("Mocchi/Pixie", [90, 100, 140, 130, 120, 110]);
baseStatsMapPrelimHard.set("Mocchi/Tiger", [90, 100, 120, 150, 110, 130]);
baseStatsMapPrelimHard.set("Mock/Joker", [180, 40, 210, 120, 80, 90]);
baseStatsMapPrelimHard.set("Mock/Mock", [160, 20, 200, 60, 50, 70]);
baseStatsMapPrelimHard.set("Monol/Gali", [140, 120, 150, 100, 20, 160]);
baseStatsMapPrelimHard.set("Monol/Golem", [140, 180, 110, 90, 10, 200]);
baseStatsMapPrelimHard.set("Monol/Hare", [130, 140, 70, 90, 110, 120]);
baseStatsMapPrelimHard.set("Monol/Jell", [140, 100, 120, 110, 10, 200]);
baseStatsMapPrelimHard.set("Monol/Monol", [160, 130, 140, 100, 10, 200]);
baseStatsMapPrelimHard.set("Monol/Naga", [150, 140, 70, 130, 30, 160]);
baseStatsMapPrelimHard.set("Monol/Plant", [180, 70, 120, 130, 20, 110]);
baseStatsMapPrelimHard.set("Monol/Pixie", [110, 100, 150, 120, 50, 120]);
baseStatsMapPrelimHard.set("Monol/Suezo", [130, 100, 160, 120, 30, 140]);
baseStatsMapPrelimHard.set("Monol/Tiger", [140, 100, 120, 110, 40, 130]);
baseStatsMapPrelimHard.set("Monol/Worm", [170, 120, 110, 100, 20, 140]);
baseStatsMapPrelimHard.set("Monol/Zuum", [140, 110, 120, 100, 30, 160]);
baseStatsMapPrelimHard.set("Naga/Gali", [120, 150, 110, 140, 70, 130]);
baseStatsMapPrelimHard.set("Naga/Golem", [130, 150, 100, 120, 90, 140]);
baseStatsMapPrelimHard.set("Naga/Hare", [130, 160, 90, 150, 120, 110]);
baseStatsMapPrelimHard.set("Naga/Jell", [110, 120, 90, 150, 100, 130]);
baseStatsMapPrelimHard.set("Naga/Monol", [120, 140, 90, 110, 100, 150]);
baseStatsMapPrelimHard.set("Naga/Naga", [120, 160, 100, 150, 110, 130]);
baseStatsMapPrelimHard.set("Naga/Pixie", [100, 130, 110, 140, 120, 90]);
baseStatsMapPrelimHard.set("Naga/Plant", [160, 120, 100, 150, 90, 70]);
baseStatsMapPrelimHard.set("Naga/Suezo", [120, 140, 110, 130, 80, 100]);
baseStatsMapPrelimHard.set("Naga/Tiger", [120, 130, 80, 160, 110, 100]);
baseStatsMapPrelimHard.set("Naga/Worm", [120, 130, 70, 150, 90, 110]);
baseStatsMapPrelimHard.set("Naga/Zuum", [110, 150, 60, 140, 110, 120]);
baseStatsMapPrelimHard.set("Niton/Bajarl", [80, 120, 60, 130, 90, 110]);
baseStatsMapPrelimHard.set("Niton/Durahan", [90, 130, 80, 70, 140, 100]);
baseStatsMapPrelimHard.set("Niton/Golem", [100, 120, 50, 60, 140, 80]);
baseStatsMapPrelimHard.set("Niton/Jell", [70, 60, 120, 110, 90, 140]);
baseStatsMapPrelimHard.set("Niton/Kato", [90, 60, 110, 80, 120, 130]);
baseStatsMapPrelimHard.set("Niton/Metalner", [80, 50, 90, 120, 70, 150]);
baseStatsMapPrelimHard.set("Niton/Mock", [80, 60, 120, 70, 40, 130]);
baseStatsMapPrelimHard.set("Niton/Niton", [110, 50, 40, 70, 60, 160]);
baseStatsMapPrelimHard.set("Phoenix/Phoenix", [170, 150, 190, 140, 160, 110]);
baseStatsMapPrelimHard.set("Pixie/Bajarl", [70, 110, 130, 150, 140, 60]);
baseStatsMapPrelimHard.set("Pixie/Centaur", [70, 80, 170, 150, 100, 60]);
baseStatsMapPrelimHard.set("Pixie/Dragon", [90, 110, 190, 140, 120, 80]);
baseStatsMapPrelimHard.set("Pixie/Durahan", [80, 110, 140, 150, 100, 130]);
baseStatsMapPrelimHard.set("Pixie/Gali", [70, 100, 170, 150, 110, 80]);
baseStatsMapPrelimHard.set("Pixie/Golem", [80, 140, 150, 110, 90, 120]);
baseStatsMapPrelimHard.set("Pixie/Hare", [80, 130, 100, 150, 140, 50]);
baseStatsMapPrelimHard.set("Pixie/Jell", [50, 80, 170, 150, 110, 60]);
baseStatsMapPrelimHard.set("Pixie/Jill", [70, 110, 170, 100, 120, 80]);
baseStatsMapPrelimHard.set("Pixie/Joker", [70, 80, 170, 150, 110, 60]);
baseStatsMapPrelimHard.set("Pixie/Kato", [50, 80, 170, 150, 140, 60]);
baseStatsMapPrelimHard.set("Pixie/Metalner", [50, 80, 100, 150, 110, 130]);
baseStatsMapPrelimHard.set("Pixie/Mock", [150, 60, 170, 100, 110, 80]);
baseStatsMapPrelimHard.set("Pixie/Monol", [110, 70, 150, 120, 100, 140]);
baseStatsMapPrelimHard.set("Pixie/Naga", [80, 120, 110, 150, 130, 80]);
baseStatsMapPrelimHard.set("Pixie/Pixie", [60, 80, 170, 150, 140, 50]);
baseStatsMapPrelimHard.set("Pixie/Plant", [100, 80, 140, 150, 110, 60]);
baseStatsMapPrelimHard.set("Pixie/Suezo", [50, 80, 170, 150, 110, 60]);
baseStatsMapPrelimHard.set("Pixie/Tiger", [70, 80, 150, 180, 140, 50]);
baseStatsMapPrelimHard.set("Pixie/Worm", [120, 70, 140, 150, 110, 60]);
baseStatsMapPrelimHard.set("Pixie/Wracky", [50, 80, 170, 100, 140, 60]);
baseStatsMapPrelimHard.set("Pixie/Zuum", [70, 80, 130, 150, 140, 90]);
baseStatsMapPrelimHard.set("Plant/Gali", [150, 60, 140, 110, 100, 70]);
baseStatsMapPrelimHard.set("Plant/Golem", [160, 120, 110, 80, 70, 130]);
baseStatsMapPrelimHard.set("Plant/Hare", [160, 120, 100, 130, 110, 60]);
baseStatsMapPrelimHard.set("Plant/Jell", [140, 50, 110, 120, 100, 80]);
baseStatsMapPrelimHard.set("Plant/Monol", [150, 90, 130, 120, 100, 110]);
baseStatsMapPrelimHard.set("Plant/Naga", [130, 110, 90, 120, 100, 70]);
baseStatsMapPrelimHard.set("Plant/Pixie", [120, 30, 140, 130, 100, 60]);
baseStatsMapPrelimHard.set("Plant/Plant", [160, 40, 110, 120, 100, 70]);
baseStatsMapPrelimHard.set("Plant/Suezo", [140, 80, 150, 110, 90, 50]);
baseStatsMapPrelimHard.set("Plant/Tiger", [140, 70, 110, 120, 90, 50]);
baseStatsMapPrelimHard.set("Plant/Worm", [170, 80, 100, 110, 90, 50]);
baseStatsMapPrelimHard.set("Plant/Zuum", [150, 80, 110, 120, 100, 80]);
baseStatsMapPrelimHard.set("Suezo/Gali", [100, 110, 190, 130, 80, 90]);
baseStatsMapPrelimHard.set("Suezo/Golem", [90, 150, 140, 130, 80, 100]);
baseStatsMapPrelimHard.set("Suezo/Hare", [80, 150, 130, 140, 110, 90]);
baseStatsMapPrelimHard.set("Suezo/Jell", [100, 110, 150, 130, 90, 120]);
baseStatsMapPrelimHard.set("Suezo/Monol", [140, 120, 150, 110, 60, 130]);
baseStatsMapPrelimHard.set("Suezo/Naga", [100, 140, 110, 160, 90, 80]);
baseStatsMapPrelimHard.set("Suezo/Pixie", [80, 120, 170, 130, 100, 90]);
baseStatsMapPrelimHard.set("Suezo/Plant", [120, 100, 150, 130, 80, 90]);
baseStatsMapPrelimHard.set("Suezo/Suezo", [80, 120, 170, 130, 90, 100]);
baseStatsMapPrelimHard.set("Suezo/Tiger", [80, 120, 160, 130, 110, 100]);
baseStatsMapPrelimHard.set("Suezo/Worm", [110, 120, 140, 130, 90, 100]);
baseStatsMapPrelimHard.set("Suezo/Zuum", [80, 120, 150, 130, 90, 100]);
baseStatsMapPrelimHard.set("Tiger/Gali", [80, 120, 130, 150, 140, 70]);
baseStatsMapPrelimHard.set("Tiger/Golem", [90, 130, 140, 110, 100, 120]);
baseStatsMapPrelimHard.set("Tiger/Hare", [80, 100, 120, 150, 140, 70]);
baseStatsMapPrelimHard.set("Tiger/Jell", [70, 90, 150, 160, 120, 80]);
baseStatsMapPrelimHard.set("Tiger/Monol", [110, 90, 140, 150, 130, 120]);
baseStatsMapPrelimHard.set("Tiger/Naga", [90, 120, 110, 190, 130, 80]);
baseStatsMapPrelimHard.set("Tiger/Pixie", [90, 80, 140, 170, 150, 60]);
baseStatsMapPrelimHard.set("Tiger/Plant", [100, 90, 120, 150, 110, 50]);
baseStatsMapPrelimHard.set("Tiger/Suezo", [90, 80, 160, 170, 110, 60]);
baseStatsMapPrelimHard.set("Tiger/Tiger", [80, 90, 130, 160, 140, 70]);
baseStatsMapPrelimHard.set("Tiger/Worm", [120, 90, 150, 130, 110, 50]);
baseStatsMapPrelimHard.set("Tiger/Zuum", [80, 90, 120, 160, 140, 100]);
baseStatsMapPrelimHard.set("Undine/Joker", [70, 90, 150, 170, 120, 60]);
baseStatsMapPrelimHard.set("Undine/Undine", [50, 10, 150, 110, 100, 60]);
baseStatsMapPrelimHard.set("Worm/Gali", [150, 110, 140, 130, 70, 80]);
baseStatsMapPrelimHard.set("Worm/Golem", [140, 190, 110, 100, 30, 120]);
baseStatsMapPrelimHard.set("Worm/Hare", [130, 140, 80, 120, 110, 90]);
baseStatsMapPrelimHard.set("Worm/Jell", [140, 100, 110, 120, 50, 130]);
baseStatsMapPrelimHard.set("Worm/Monol", [20, 120, 110, 130, 30, 100]);
baseStatsMapPrelimHard.set("Worm/Naga", [150, 140, 90, 130, 50, 100]);
baseStatsMapPrelimHard.set("Worm/Pixie", [130, 100, 160, 120, 90, 70]);
baseStatsMapPrelimHard.set("Worm/Plant", [180, 90, 120, 130, 50, 100]);
baseStatsMapPrelimHard.set("Worm/Suezo", [150, 110, 130, 120, 60, 90]);
baseStatsMapPrelimHard.set("Worm/Tiger", [130, 100, 110, 150, 90, 80]);
baseStatsMapPrelimHard.set("Worm/Worm", [180, 110, 100, 120, 60, 90]);
baseStatsMapPrelimHard.set("Worm/Zuum", [150, 110, 100, 130, 90, 70]);
baseStatsMapPrelimHard.set("Wracky/Bajarl", [80, 70, 120, 90, 150, 50]);
baseStatsMapPrelimHard.set("Wracky/Dragon", [90, 100, 150, 80, 110, 70]);
baseStatsMapPrelimHard.set("Wracky/Durahan", [50, 40, 140, 60, 120, 100]);
baseStatsMapPrelimHard.set("Wracky/Golem", [80, 120, 150, 60, 100, 110]);
baseStatsMapPrelimHard.set("Wracky/Henger", [40, 70, 140, 90, 150, 50]);
baseStatsMapPrelimHard.set("Wracky/Joker", [80, 70, 130, 100, 120, 50]);
baseStatsMapPrelimHard.set("Wracky/Metalner", [60, 50, 120, 100, 130, 110]);
baseStatsMapPrelimHard.set("Wracky/Mock", [70, 40, 150, 50, 120, 60]);
baseStatsMapPrelimHard.set("Wracky/Pixie", [60, 30, 140, 90, 150, 50]);
baseStatsMapPrelimHard.set("Wracky/Wracky", [20, 10, 150, 40, 160, 30]);
baseStatsMapPrelimHard.set("Zilla/Jell", [100, 150, 120, 80, 90, 110]);
baseStatsMapPrelimHard.set("Zilla/Pixie", [120, 150, 110, 70, 100, 90]);
baseStatsMapPrelimHard.set("Zilla/Tiger", [140, 160, 130, 110, 120, 100]);
baseStatsMapPrelimHard.set("Zilla/Zilla", [150, 180, 80, 50, 60, 100]);
baseStatsMapPrelimHard.set("Zuum/Arrowhead", [130, 120, 80, 150, 110, 140]);
baseStatsMapPrelimHard.set("Zuum/Bajarl", [130, 120, 80, 150, 100, 110]);
baseStatsMapPrelimHard.set("Zuum/Baku", [130, 120, 80, 110, 90, 100]);
baseStatsMapPrelimHard.set("Zuum/Dragon", [90, 120, 110, 140, 100, 80]);
baseStatsMapPrelimHard.set("Zuum/Gali", [140, 130, 100, 150, 110, 120]);
baseStatsMapPrelimHard.set("Zuum/Golem", [110, 140, 80, 120, 100, 130]);
baseStatsMapPrelimHard.set("Zuum/Hare", [120, 140, 80, 150, 130, 100]);
baseStatsMapPrelimHard.set("Zuum/Jell", [120, 130, 100, 140, 110, 140]);
baseStatsMapPrelimHard.set("Zuum/Joker", [120, 100, 90, 140, 130, 110]);
baseStatsMapPrelimHard.set("Zuum/Kato", [120, 90, 130, 140, 100, 110]);
baseStatsMapPrelimHard.set("Zuum/Mock", [90, 130, 100, 120, 140, 110]);
baseStatsMapPrelimHard.set("Zuum/Monol", [110, 100, 80, 120, 90, 150]);
baseStatsMapPrelimHard.set("Zuum/Naga", [130, 100, 60, 150, 120, 110]);
baseStatsMapPrelimHard.set("Zuum/Pixie", [70, 110, 120, 140, 100, 90]);
baseStatsMapPrelimHard.set("Zuum/Plant", [130, 90, 80, 140, 120, 100]);
baseStatsMapPrelimHard.set("Zuum/Suezo", [140, 110, 100, 150, 120, 130]);
baseStatsMapPrelimHard.set("Zuum/Tiger", [130, 120, 100, 150, 110, 90]);
baseStatsMapPrelimHard.set("Zuum/Worm", [150, 110, 80, 130, 70, 120]);
baseStatsMapPrelimHard.set("Zuum/Zuum", [130, 120, 80, 140, 100, 110]);

var baseStatsListHard = Array.from(baseStatsMapPrelimHard.entries());
var baseStatsMapHard = new Map();

baseStatsListHard.forEach(function (element) {
  baseStatsMapHard.set(element[0], [
    { label: "LIF", value: element[1][0] },
    { label: "POW", value: element[1][1] },
    { label: "INT", value: element[1][2] },
    { label: "SKL", value: element[1][3] },
    { label: "SPD", value: element[1][4] },
    { label: "DEF", value: element[1][5] },
  ]);
});
