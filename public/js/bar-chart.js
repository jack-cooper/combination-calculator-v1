Vue.component('bar-chart', {
  template: '#bar-chart-template',
  props: {
    size: {
      type: Number
    },
    barValues: {
      type: Array,
      default: function () {
        return [
          { label: 'LIF', value: 0 },
          { label: 'POW', value: 0 },
          { label: 'INT', value: 0 },
          { label: 'SKL', value: 0 },
          { label: 'SPD', value: 0 },
          { label: 'DEF', value: 0 },
        ]
      }
    },
    maxValue: {
      type: Number
    }
  }
})
